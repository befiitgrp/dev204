@extends('layouts.app')

@section('content')


    <div class="background-image grid grid-cols-1 m-auto">
        <div class="flex text-gray-200 pt-10">
            <div class="m-auto pt-4 pb-16 sm:m-auto w-4/5 block text-center">
                <h1 class="sm:text-white text-5xl uppercase font-bold pb-14">
                    WELCOME TO THE FUTURE OF DEVELOPMENT
                </h1>
                <a href="/blog" class="text-center bg-gradient-to-r from-purple-600 to-pink-600 text-white py-3 px-8 font-bold text-xl uppercase rounded-full shadow-md hover:shadow-lg transition duration-300 ease-in-out">
                    Explore Now
                </a>
            </div>
        </div>
    </div>

    <div class="sm:grid grid-cols-2 gap-20 w-4/5 mx-auto py-15 border-b border-gray-300">
        <div>
            <img src="https://cdn.pixabay.com/photo/2014/05/03/01/03/laptop-336704_960_720.jpg" width="700" alt="">
        </div>

        <div class="m-auto sm:m-auto text-left w-4/5 block">
            <h2 class="text-3xl font-extrabold text-gray-400">
                UNLEASH YOUR POTENTIAL WITH US
            </h2>

            <p class="py-8 text-gray-300 text-lg">
                Experience the cutting-edge resources and expertise to propel your development journey into the future.
            </p>

            <p class="font-extrabold text-gray-400 text-lg pb-9">
                Seize the opportunity now!
            </p>

            <a href="/blog" class="uppercase bg-gradient-to-r from-purple-600 to-pink-600 text-white py-3 px-8 font-bold text-lg rounded-full shadow-md hover:shadow-lg transition duration-300 ease-in-out">
                Learn More
            </a>
        </div>
    </div>

    <div class="text-center p-15 bg-gray-900 text-white">
        <h2 class="text-2xl pb-5 text-l"> 
            EMBRACE THE FUTURE OF DEVELOPMENT IN...
        </h2>

        <span class="font-extrabold block text-4xl py-1">
            UX Design
        </span>
        <span class="font-extrabold block text-4xl py-1">
            Project Management
        </span>
        <span class="font-extrabold block text-4xl py-1">
            Cybersecurity
        </span>
        <span class="font-extrabold block text-4xl py-1">
            Full Stack Development
        </span>
    </div>

    <div class="text-center py-15">
        <span class="uppercase text-xs text-gray-400">
            Blog
        </span>

        <h2 class="text-4xl font-bold py-10 text-gray-300">
            LATEST POSTS
        </h2>

        <p class="m-auto w-4/5 text-gray-400">
            Dive into our latest articles covering the forefront of technology, innovation, and development trends.
        </p>
    </div>

    <div class="sm:grid grid-cols-2 w-4/5 m-auto">
        <div class="flex bg-gradient-to-r from-purple-600 to-pink-600 text-white pt-10">
            <div class="m-auto pt-4 pb-16 sm:m-auto w-4/5 block">
                <span class="uppercase text-xs">
                    ReactJS
                </span>

                <h3 class="text-xl font-bold py-10">
                    Elevate your web development with React.js, the ultimate framework for building futuristic user interfaces. Whether you're crafting a dynamic web application or a responsive website, React.js empowers you with unparalleled flexibility and performance.
                </h3>

                <a href="#" class="uppercase bg-transparent border-2 border-white text-white text-xs font-bold py-3 px-5 rounded-full shadow-md hover:shadow-lg transition duration-300 ease-in-out">
                    Discover More
                </a>
            </div>
        </div>
        <div>
            <img src="https://cdn.pixabay.com/photo/2014/05/03/01/03/laptop-336704_960_720.jpg" alt="">
        </div>
    </div>
   
</div>

@endsection

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    
    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body class="bg-gray-100 h-screen antialiased leading-none font-sans">
    <div id="app">
        <header class="bg-purple-800 py-6">
            <div class="container mx-auto flex justify-between items-center px-6">
                <div>
                    <a href="{{ url('/') }}" class="text-lg font-semibold text-white no-underline">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>
                <nav>
                    <ul class="flex">
                        <li class="mr-6"><a href="/" class="text-white hover:text-gray-300">Home</a></li>
                        <li class="mr-6"><a href="/blog" class="text-white hover:text-gray-300">Blog</a></li>
                        @guest
                            <li class="mr-6"><a href="{{ route('login') }}" class="text-white hover:text-gray-300">{{ __('Login') }}</a></li>
                            @if (Route::has('register'))
                                <li><a href="{{ route('register') }}" class="text-white hover:text-gray-300">{{ __('Register') }}</a></li>
                            @endif
                        @else
                            <li class="mr-6 text-white">{{ Auth::user()->name }}</li>

                            <li><a href="{{ route('logout') }}"
                               class="text-white hover:text-gray-300"
                               onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">{{ __('Logout') }}</a></li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                                {{ csrf_field() }}
                            </form>
                        @endguest
                    </ul>
                </nav>
            </div>
        </header>

        <div>
            @yield('content')
        </div>

        <footer class="bg-purple-800 text-white py-8">
    <div class="container mx-auto flex justify-between items-center">
        <div class="logo">
            @guest
                <h2 class="text-2xl font-bold">Welcome </h2>
            @else
                <h2 class="text-2xl font-bold">Welcome {{ Auth::user()->name }}</h2>
            @endguest
        </div>
        <div class="links">
            <ul class="flex">
                <li class="mr-6"><a href="#" class="hover:text-gray-300">Home</a></li>
                <li class="mr-6"><a href="#" class="hover:text-gray-300">About</a></li>
                <li class="mr-6"><a href="#" class="hover:text-gray-300">Services</a></li>
                <li><a href="#" class="hover:text-gray-300">Contact</a></li>
            </ul>
        </div>
        <div class="social">
            <ul class="flex">
                <li class="mr-4"><a href="#" class="hover:text-gray-300"><i class="fab fa-facebook-f"></i></a></li>
                <li class="mr-4"><a href="#" class="hover:text-gray-300"><i class="fab fa-twitter"></i></a></li>
                <li><a href="#" class="hover:text-gray-300"><i class="fab fa-instagram"></i></a></li>
            </ul>
        </div>
    </div>
</footer>


    </div>
</body>
</html>


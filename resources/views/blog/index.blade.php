@extends('layouts.app')

@section('content')
<div class="w-4/5 m-auto text-center">
    <div class="py-15 border-b border-gray-200">
        <h1 class="text-6xl font-bold text-gray-900">
            Blog Posts
        </h1>
    </div>
</div>

@if (session()->has('message'))
    <div class="w-4/5 m-auto mt-10 pl-2">
        <p class="w-2/3 mb-4 text-gray-100 bg-green-500 rounded-lg py-4 px-6">
            {{ session()->get('message') }}
        </p>
    </div>
@endif

@if (Auth::check())
    <div class="pt-8 w-4/5 m-auto">
        <a 
            href="/blog/create"
            class="uppercase bg-gradient-to-r from-purple-600 to-pink-600 text-white text-xs font-extrabold py-3 px-8 rounded-full shadow-md hover:shadow-lg transition duration-300 ease-in-out">
            Create Post
        </a>
    </div>
@endif

@foreach ($posts as $post)
    <div class="sm:flex justify-between items-center w-4/5 mx-auto py-12 border-b border-gray-300">
        <div class="sm:w-1/2 mb-8 sm:mb-0">
            <img src="{{ asset('images/' . $post->image_path) }}" alt="{{ $post->title }}" class="w-full h-auto rounded-lg">
        </div>
        <div class="sm:w-1/2 sm:pl-8">
            <h2 class="text-4xl font-bold text-gray-900 mb-4">
                {{ $post->title }}
            </h2>

            <p class="text-gray-700 text-lg mb-4">
                By <span class="font-bold italic text-gray-800">{{ $post->user->name }}</span>, Created on {{ date('jS M Y', strtotime($post->updated_at)) }}
            </p>

            <p class="text-gray-800 text-lg leading-8 mb-6">
                {{ $post->description }}
            </p>

            <a href="/blog/{{ $post->slug }}" class="uppercase bg-gradient-to-r from-purple-600 to-pink-600 text-white text-lg font-extrabold py-3 px-8 rounded-full shadow-md hover:shadow-lg transition duration-300 ease-in-out">
                Read More
            </a>

            @if (isset(Auth::user()->id) && Auth::user()->id == $post->user_id)
                <div class="mt-4 text-right">
                    <a 
                        href="/blog/{{ $post->slug }}/edit"
                        class="text-gray-700 italic hover:text-gray-900 pb-1 border-b-2 border-transparent hover:border-gray-500 mr-4">
                        Edit
                    </a>

                    <form 
                        action="/blog/{{ $post->slug }}"
                        method="POST">
                        @csrf
                        @method('delete')

                        <button
                            class="text-red-500 italic hover:text-red-700 border-none focus:outline-none"
                            type="submit">
                            Delete
                        </button>

                    </form>
                </div>
            @endif
        </div>
    </div>    
@endforeach

@endsection

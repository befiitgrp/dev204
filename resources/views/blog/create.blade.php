@extends('layouts.app')

@section('content')
<div class="w-4/5 m-auto text-left">
    <div class="py-15">
        <h1 class="text-6xl font-bold text-gray-900">
            Create Post
        </h1>
    </div>
</div>
 
@if ($errors->any())
    <div class="w-4/5 m-auto">
        <ul>
            @foreach ($errors->all() as $error)
                <li class="w-4/5 mb-4 text-gray-900 bg-red-400 rounded-lg py-2 px-4">
                    {{ $error }}
                </li>
            @endforeach
        </ul>
    </div>
@endif

<div class="w-4/5 m-auto pt-8">
    <form 
        action="/blog"
        method="POST"
        enctype="multipart/form-data"
        class="bg-gray-100 shadow-md rounded-lg px-8 pt-6 pb-8 mb-4">
        @csrf

        <input 
            type="text"
            name="title"
            placeholder="Title..."
            class="bg-white block border-2 border-gray-300 w-full h-20 text-4xl rounded-lg mb-4 px-4 outline-none">

        <textarea 
            name="description"
            placeholder="Description..."
            class="bg-white block border-2 border-gray-300 w-full h-60 text-lg rounded-lg mb-4 px-4 py-2 outline-none"></textarea>

        <div class="bg-gray-200 p-4 rounded-lg mb-4">
            <label class="text-gray-700 font-bold text-lg mb-2">
                Upload Image
            </label>
            <label class="w-full flex items-center justify-center px-4 py-6 bg-white text-gray-900 rounded-lg tracking-wide uppercase border border-blue cursor-pointer hover:bg-blue-500 hover:text-white">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path>
                </svg>
                <span class="mt-2 text-base leading-normal">
                    Select a file
                </span>
                <input 
                    type="file"
                    name="image"
                    class="hidden">
            </label>
        </div>

        <button    
            type="submit"
            class="uppercase bg-blue-500 text-white text-lg font-bold py-4 px-8 rounded-lg hover:bg-blue-600">
            Submit Post
        </button>
    </form>
</div>

@endsection
